export type MagmaOp<X> = (head: X, ...tail: X[]) => X;

export interface FreeMagmaElement<X> {
  foldMap<Y>(leaf: (m: X) => Y, branch: MagmaOp<Y>): Y;
  forEach(callbackFn: (m: X) => any, thisArg?: any): void;
}

export class MagmaElement<X> implements FreeMagmaElement<X> {
  constructor(public value: X) {}
  foldMap<Y>(f: (m: X) => Y, _ignored: any): Y {
    return f(this.value);
  }
  forEach(callbackFn: (m: X) => any, thisArg?: any): void {
    callbackFn.apply(thisArg, [this.value]);
  }
}

export class MagmaOpTree<X> implements FreeMagmaElement<X> {
  constructor(
    public head: FreeMagmaElement<X>,
    ...tail: FreeMagmaElement<X>[]
  ) {
    this.tail = tail;
  }
  public tail: FreeMagmaElement<X>[];
  static fromElements<Y>(head: Y, ...tail: Y[]) {
    return new MagmaOpTree(
      new MagmaElement(head),
      ...tail.map((m) => new MagmaElement(m))
    );
  }
  foldMap<Y>(leaf: (m: X) => Y, branch: MagmaOp<Y>): Y {
    return branch(
      this.head.foldMap(leaf, branch),
      ...this.tail.map((tree) => tree.foldMap(leaf, branch))
    );
  }
  forEach(callbackFn: (m: X) => any, thisArg?: any): void {
    this.head.forEach(callbackFn, thisArg);
    this.tail.forEach((tree) => tree.forEach(callbackFn, thisArg));
  }
}

export function magmaElementToArray<X>(magma: FreeMagmaElement<X>): X[] {
  const accum: X[] = [];
  magma.forEach((e) => accum.push(e));
  return accum;
}

export function concatMany(strs: FreeMagmaElement<string>): string {
  return magmaElementToArray(strs).join("");
}
