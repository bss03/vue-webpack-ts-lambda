import { Component, Prop, Vue } from "vue-property-decorator";
import { Expression, asExpression, minify } from "..//utlc";

@Component
export default class MinifierDisplay extends Vue {
  @Prop(Object) expr: Expression | undefined;
  get minification(): string {
    const expr = asExpression(this.expr);
    if (null === expr) {
      return "[ERROR: Bad Expression]";
    }

    return minify(expr);
  }
}
