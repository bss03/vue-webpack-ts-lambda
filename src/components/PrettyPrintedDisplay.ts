import { Component, Prop, Vue } from "vue-property-decorator";
import { Expression, asExpression } from "..//utlc";
import { prettyPrint } from "../prettier";

@Component
export default class PrettyPrintedDisplay extends Vue {
  @Prop(Object) expr: Expression | undefined;
  get prettyExprStr(): string {
    const expr = asExpression(this.expr);
    if (null === expr) {
      return "[ERROR: Bad Expression]";
    }

    return prettyPrint(expr);
  }
}
