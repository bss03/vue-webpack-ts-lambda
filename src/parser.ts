import {
  between,
  char,
  choice,
  endOfInput,
  letters,
  many,
  optionalWhitespace,
  possibly,
  recursiveParser,
  sepBy,
  sequenceOf,
  succeedWith,
  takeLeft,
  takeRight,
  whitespace,
} from "arcsecond";

import { Variable, Abstraction, Application, Expression } from "./utlc";

const varParser = letters.map((name: string) => new Variable(name));

export const exprParser = recursiveParser(() =>
  argumentsParser.map(([head, tail]: [Expression, Expression[]]) =>
    tail.reduce((f, a) => new Application(f, a), head)
  )
);

const parenthesizedExprParser = between(
  sequenceOf([char("("), optionalWhitespace])
)(sequenceOf([optionalWhitespace, char(")")]))(exprParser);

const absParser = sequenceOf([
  takeRight(sequenceOf([char("\\"), optionalWhitespace]))(letters),
  takeRight(sequenceOf([optionalWhitespace, char("."), optionalWhitespace]))(
    exprParser
  ),
]).map(
  ([parameter, body]: [string, Expression]) => new Abstraction(parameter, body)
);

const argumentParser = choice([parenthesizedExprParser, varParser]);
const argumentsParser = possibly(argumentParser)
  .chain((head: Expression | null) => {
    if (null === head) {
      return succeedWith(null);
    }
    return many(takeRight(whitespace)(argumentParser)).map(
      (tail: Expression[]) => [head, tail]
    );
  })
  .chain((arguments_: [Expression, Expression[]] | null) => {
    if (null == arguments_) {
      return absParser.map((argument: Abstraction) => [argument, []]);
    }

    return possibly(takeRight(whitespace)(absParser)).map(
      (last: Abstraction | null) => {
        if (null === last) {
          return arguments_;
        }

        const [head, tail] = arguments_;
        return [head, tail.concat(last)];
      }
    );
  });

export function parseExpr(input: string) {
  return takeLeft(exprParser)(endOfInput).run(input);
}
