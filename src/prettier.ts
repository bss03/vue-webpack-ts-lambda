import {
  FreeMagmaElement,
  MagmaElement,
  MagmaOpTree,
  concatMany,
} from "./Magma";
import { Expression, Abstraction, Application, Variable } from "./utlc";

function ppVarElem(var_: Variable): MagmaElement<string> {
  return new MagmaElement(var_.name);
}
function ppAbsTree(abs: Abstraction, indent: number): FreeMagmaElement<string> {
  return new MagmaOpTree(
    MagmaOpTree.fromElements("(\\", abs.parameter, ". "),
    ppTree(abs.body, indent + abs.parameter.length + 4),
    new MagmaElement(")")
  );
}
function ppAppTree(app: Application, indent: number): FreeMagmaElement<string> {
  return new MagmaOpTree(
    ppTree(app.function_, indent),
    MagmaOpTree.fromElements("\n", " ".repeat(indent)),
    app.argument.case(
      (abs) => ppAbsTree(abs, indent),
      (app) =>
        new MagmaOpTree(
          new MagmaElement("("),
          ppAppTree(app, indent + 1),
          new MagmaElement(")")
        ),
      (var_) => ppVarElem(var_)
    )
  );
}
function ppTree(
  expr: Expression,
  indent: number = 0
): FreeMagmaElement<string> {
  return expr.case(
    (abs) => ppAbsTree(abs, indent),
    (app) => ppAppTree(app, indent),
    (var_) => ppVarElem(var_)
  );
}
export function prettyPrint(expr: Expression): string {
  return concatMany(ppTree(expr));
}
