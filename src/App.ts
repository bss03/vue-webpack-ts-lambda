import { Component, Vue } from "vue-property-decorator";
import MinifierDisplay from "./components/MinifierDisplay.vue";
import PrettyPrintedDisplay from "./components/PrettyPrintedDisplay.vue";
import { Expression, minify } from "./utlc";
import { parseExpr } from "./parser";

@Component({
  components: {
    MinifierDisplay,
    PrettyPrintedDisplay,
  },
})
export default class App extends Vue {
  exprStr: string = "(\\x. x x)";
  private _lastGoodExpr: Expression | null = null;
  get parseResult() {
    return parseExpr(this.exprStr);
  }
  get parseError(): string {
    if (this.parseResult.isError) {
      return this.parseResult.error;
    }

    return "";
  }
  get expr(): Expression | null {
    if (this.parseResult.isError) {
      return null;
    }
    return this.parseResult.result;
  }
  get lastGoodExpr(): Expression | null {
    if (this.expr) {
      this._lastGoodExpr = this.expr;
    }
    return this._lastGoodExpr;
  }
}
