/* Untyped Lambda Calculus */
import {
  concatMany,
  MagmaElement,
  MagmaOpTree,
  FreeMagmaElement,
} from "./Magma";

export interface Expression {
  case<Result>(
    caseAbs: (abs: Abstraction) => Result,
    caseApp: (app: Application) => Result,
    caseVar: (var_: Variable) => Result
  ): Result;
}

export function asExpression(object: any): Expression | null {
  return object && object.case ? <Expression>object : null;
}

export class Abstraction implements Expression {
  constructor(public parameter: string, public body: Expression) {}
  case<Result>(f: (abs: Abstraction) => Result, ignored1: any, ignored2: any) {
    return f(this);
  }
}

export class Application implements Expression {
  constructor(public function_: Expression, public argument: Expression) {}
  case<Result>(ignored1: any, f: (app: Application) => Result, ignored2: any) {
    return f(this);
  }
}

export class Variable implements Expression {
  constructor(public name: string) {}
  case<Result>(ignored1: any, ignored2: any, f: (var_: Variable) => Result) {
    return f(this);
  }
}

export type Value = Abstraction;

const name = "x";
const var_ = new Variable(name);
const app = new Application(var_, var_);
const selfApply = new Abstraction(name, app);
export const example = new Application(selfApply, selfApply);

function minifyVar(var_: Variable): string {
  return var_.name;
}

function minifyAbs(abs: Abstraction): FreeMagmaElement<string> {
  return new MagmaOpTree(
    MagmaOpTree.fromElements("\\", abs.parameter, "."),
    minifyTree(abs.body)
  );
}

function minifyApp(
  app: Application,
  parenAbsArg: boolean = false
): FreeMagmaElement<string> {
  function parenthesize(tree: FreeMagmaElement<string>) {
    return new MagmaOpTree(new MagmaElement("("), tree, new MagmaElement(")"));
  }
  const fnTree = app.function_.case(
    (abs) => parenthesize(minifyAbs(abs)),
    (app) => minifyApp(app, true),
    (var_) => new MagmaElement(minifyVar(var_))
  );
  const argTree = app.argument.case(
    (abs) => {
      const minAbs = minifyAbs(abs);
      if (parenAbsArg) {
        return parenthesize(minAbs);
      }
      return minAbs;
    },
    (app) => parenthesize(minifyApp(app)),
    (var_) => new MagmaElement(minifyVar(var_))
  );
  return new MagmaOpTree(fnTree, new MagmaElement(" "), argTree);
}

export function minifyTree(expr: Expression): FreeMagmaElement<string> {
  return expr.case(
    minifyAbs,
    minifyApp,
    (var_) => new MagmaElement(minifyVar(var_))
  );
}
export function minify(expr: Expression): string {
  return concatMany(minifyTree(expr));
}
