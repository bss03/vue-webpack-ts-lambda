import {
  MagmaElement,
  MagmaOpTree,
  magmaElementToArray,
  concatMany,
} from "@/Magma";

const wrapped = new Object();
const nullFn = () => undefined;
const me = new MagmaElement(wrapped);

describe("MagmaElement.foldMap", () => {
  it("calls the 'leaf' argument once with the wrapped object", () => {
    const leaf = jest.fn();
    me.foldMap(leaf, nullFn);
    expect(leaf).toHaveBeenCalledTimes(1);
    expect(leaf).toHaveBeenCalledWith(wrapped);
  });
  it("ignores the 'branch' argument", () => {
    const branch = jest.fn();
    me.foldMap(nullFn, branch);
    expect(branch).not.toHaveBeenCalled();
  });
});

describe("MagmaElement.forEach", () => {
  it("calls the 'callbackFn' once with the wrapped object on 'thisArg'", () => {
    const thisArg = new Object();
    const callbackFn = jest.fn(function (this: any) {
      expect(this).toBe(thisArg);
    });
    me.forEach(callbackFn, thisArg);
    expect(callbackFn).toHaveBeenCalledTimes(1);
    expect(callbackFn).toHaveBeenCalledWith(wrapped);
  });
  it("defaults 'thisArg' to undefined", () => {
    me.forEach(function (this: any) {
      expect(this).toBe(undefined);
    });
  });
});

const x = new Object();
const y = new Object();
const mot = new MagmaOpTree(me, MagmaOpTree.fromElements(x, y));

describe("MagmaOpTree.foldMap", () => {
  it("calls the 'leaf' argument with each contained object", () => {
    const leaf = jest.fn();
    const nullOp = (_head: any, ..._tail: any) => undefined;
    mot.foldMap(leaf, nullOp);
    expect(leaf).toHaveBeenCalledTimes(3);
    expect(leaf).toHaveBeenCalledWith(wrapped);
    expect(leaf).toHaveBeenCalledWith(x);
    expect(leaf).toHaveBeenCalledWith(y);
  });
  it("calls the 'branch' argument at least once", () => {
    const branch = jest.fn();
    mot.foldMap(nullFn, branch);
    expect(branch).toHaveBeenCalled();
  });
});
describe("MagamOpTree.forEach", () => {
  it("calls the 'callbackFn' with each contained object on 'thisArg'", () => {});
  it("defaults 'thisArg' to undefined", () => {});
});

describe("magmaElementToArray", () => {
  it("is the same as accumulating with forEach", () => {
    const expected = [wrapped, x, y];
    const result = magmaElementToArray(mot);
    expect(result).toEqual(expect.arrayContaining(expected));
    expect(expected).toEqual(expect.arrayContaining(result));
  });
});
describe("concatMany", () => {
  it("is the same as magmaElementToArray then join", () => {
    expect(concatMany(new MagmaElement("foo"))).toEqual("foo");
    expect(
      concatMany(
        new MagmaOpTree(new MagmaElement("foo"), new MagmaElement("bar"))
      )
    ).toEqual("foobar");
    expect(concatMany(MagmaOpTree.fromElements("foo", "bar", "baz"))).toEqual(
      "foobarbaz"
    );
  });
});
